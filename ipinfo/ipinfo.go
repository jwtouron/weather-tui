package ipinfo

import (
	"gitlab.com/jwtouron/weather-tui/util"
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"strings"
)

type IPInfo struct {
	Lat, Long float64
}

func Fetch() (IPInfo, error) {
	resp, err := http.Get("http://ipinfo.io")
	if err != nil {
		return IPInfo{}, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return IPInfo{}, err
	}

	m := map[string]string{}
	err = json.Unmarshal(body, &m)
	util.Assert(err == nil, "Can't unmarshal ipinfo JSON: %v", err)

	loc := m["loc"]
	splits := strings.Split(loc, ",")
	lat, err := strconv.ParseFloat(splits[0], 64)
	util.Assert(err == nil, "Couldn't convert lat to float64: %v", loc)
	long, err := strconv.ParseFloat(splits[1], 64)
	util.Assert(err == nil, "Couldn't convert lat to float64: %v", loc)

	return IPInfo{lat, long}, nil
}
