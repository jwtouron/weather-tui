package util

import "fmt"

func Assert(cond bool, format string, a ...any) {
	if !cond {
		panic(fmt.Sprintf(format, a...))
	}
}
