package ui

import (
	"fmt"
	"reflect"

	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/jwtouron/weather-tui/ipinfo"
	"gitlab.com/jwtouron/weather-tui/nws"
)

type WeatherDataMsg interface{}
type latestObservation struct {
	station nws.ObservationStation
	observation interface{}
}

type currentError error
type forecastError error

type WeatherData struct {
	data interface{}
}

type weatherData struct {
	currentIndex int
	current interface{}
	forecast interface{}
}

func (df WeatherData) Init() tea.Cmd {
	return func() tea.Msg {
		ipinfo, err := ipinfo.Fetch()
		if err == nil {
			return ipinfo
		} else {
			return err
		}
	}
}

func (wd WeatherData) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd = nil
	switch msg := msg.(type) {
	case WeatherDataMsg:
		switch msg := msg.(type) {
		case error:
			wd.data = msg
		case ipinfo.IPInfo:
			cmd = fetchPoint(msg)
		case nws.Point:
			wd.data = weatherData{}
			cmd = fetchWeatherData(msg)
		case currentError:
			data := wd.data.(weatherData)
			data.current = msg
			wd.data = data
		case forecastError:
			data := wd.data.(weatherData)
			data.forecast = msg
			wd.data = data
		case []nws.ObservationStation:
			cmd = fetchLatestObservations(msg)
		case []nws.PeriodForecast:
			data := wd.data.(weatherData)
			data.forecast = msg
			wd.data = data
		case []latestObservation:
			data := wd.data.(weatherData)
			data.current = msg
			wd.data = data
		default:
			panic(fmt.Sprintf("Unknown message type: %v", msg))
		}
	}
	return wd, cmd
}

func (wd WeatherData) UpdateCurrent(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "h":
			if wd2, ok := wd.data.(weatherData); ok {
				if los, ok := wd2.current.([]latestObservation); ok {
					if wd2.currentIndex == 0 {
						wd2.currentIndex = len(los) - 1
					} else {
						wd2.currentIndex = (wd2.currentIndex - 1) % len(los)
					}
					wd.data = wd2
				}
			}
		case "l":
			if wd2, ok := wd.data.(weatherData); ok {
				if los, ok := wd2.current.([]latestObservation); ok {
					wd2.currentIndex = (wd2.currentIndex + 1) % len(los)
					wd.data = wd2
				}
			}
		}
	}
	return wd, nil
}

func (wd WeatherData) View() string {
	return ""
}

func (wd WeatherData) CurrentView() string {
	switch data := wd.data.(type) {
	case error:
		return fmt.Sprintf("%v", data)
	case weatherData:
		switch current := data.current.(type) {
		case error:
			return fmt.Sprintf("%v", current)
		case []latestObservation:
			obs := &current[data.currentIndex]
			result := fmt.Sprintf(
				"%s (%s)\n\n",
				obs.station.Name(),
				obs.station.StationIdentifier(),
			)
			if lo, ok := obs.observation.(nws.LatestObservation); ok {
				if temp, ok := lo.Temperature().Fahrenheit(); ok {
					if hi, ok := lo.HeatIndex().Fahrenheit(); ok {
						result += fmt.Sprintf("%-25s%.2f°F (%.2f°F)\n", "Temperature:", temp, hi)
					} else {
						result += fmt.Sprintf("%-25s%.2f°F\n", "Temperature:", temp)
					}
				} else {
					result += "Temperature:\n"
				}
				if hum, ok := lo.RelativeHumidity(); ok {
					result += fmt.Sprintf("%-25s%.2f%%\n", "Relative Humidity:", hum)
				} else {
					result += fmt.Sprintf("%-25s\n", "Relative Humidity:")
				}
				result += fmt.Sprintf("%-25s%s", "Description:", lo.TextDescription())
			} else {
				result += (obs.observation.(error)).Error()
			}
			return result
		}
	}
	return "Fetching data..."
}

func (wd WeatherData) ForecastView() string {
	switch data := wd.data.(type) {
	case nil:
		return "Fetching data..."
	case error:
		return fmt.Sprintf("%v", data)
	default:
		return fmt.Sprintf("something else: %v", reflect.TypeOf(data))
	}
}

func fetchPoint(ipinfo ipinfo.IPInfo) tea.Cmd {
	return func() tea.Msg {
		point, err := nws.FetchPoint(ipinfo.Lat, ipinfo.Long)
		if err == nil {
			return point
		} else {
			return nil
		}
	}
}

func fetchWeatherData(point nws.Point) tea.Cmd {
	return tea.Batch(
		func() tea.Msg {
			stations, err := point.FetchObservationStations()
			if err == nil {
				return stations
			} else {
				return err.(currentError)
			}
		},
		func() tea.Msg {
			periodForecasts, err := point.FetchHourlyForecast()
			if err == nil {
				return periodForecasts
			} else {
				return err.(forecastError)
			}
		},
	)
}

func fetchLatestObservations(stations []nws.ObservationStation) tea.Cmd {
	return func() tea.Msg {
		observations := make([]latestObservation, 0, len(stations))
		ch := make(chan latestObservation)
		for _, station := range stations {
			station := station
			go func() {
				result := latestObservation{}
				result.station = station
				obs, err := station.FetchLatestObservation()
				if err == nil {
					result.observation = obs
				} else {
					result.observation = err
				}
				ch <- result
			}()
		}
		for i := 0; i < len(stations); i++ {
			observations = append(observations, <-ch)
		}
		return observations
	}
}
