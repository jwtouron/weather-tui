package main

import (
	"fmt"
	"os"

	"gitlab.com/jwtouron/weather-tui/ui"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	// "github.com/charmbracelet/bubbles/viewport"
)

type model struct {
	activePane int
	windowSizeMsg tea.WindowSizeMsg
	weatherData ui.WeatherData
	// viewport viewport.Model
}

func (m model) Init() tea.Cmd {
	return m.weatherData.Init()
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd = nil

	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.String() {
		case "tab":
			m.activePane = (m.activePane + 1) % 2
		case "ctrl+c", "q":
			cmd = tea.Quit
		default:
			if m.activePane == 0 {
				m2, c := m.weatherData.UpdateCurrent(msg)
				m.weatherData = m2.(ui.WeatherData)
				cmd = c
			}
		}
	case tea.WindowSizeMsg:
		m.windowSizeMsg = msg
		// m.viewport = viewport.New(m.windowSizeMsg.Width - 2, (m.windowSizeMsg.Height + 1) / 2 - 2)
	case ui.WeatherDataMsg:
		var wd interface{}
		wd, cmd = m.weatherData.Update(msg)
		m.weatherData = wd.(ui.WeatherData)
	}

	return m, cmd
}

func (m model) View() string {
	paneStyle := lipgloss.NewStyle().
		Width(m.windowSizeMsg.Width - 2).
		BorderStyle(lipgloss.NormalBorder())
	currentPaneStyle := lipgloss.NewStyle().
		Height(m.windowSizeMsg.Height / 2 - 2).
		Inherit(paneStyle)
	forecastPaneStyle := lipgloss.NewStyle().
		Height(m.windowSizeMsg.Height / 2 - 2).
		Inherit(paneStyle)

	if m.activePane == 0 {
		currentPaneStyle.BorderForeground(lipgloss.Color("#00FF00"))
	} else {
		forecastPaneStyle.BorderForeground(lipgloss.Color("#00FF00"))
	}

	// m.viewport.SetContent(bottomPane)

	s := lipgloss.JoinVertical(
		lipgloss.Left,
		currentPaneStyle.Render(
			m.weatherData.CurrentView(),
		),
		forecastPaneStyle.Render(
			m.weatherData.ForecastView(),
		),
	)
	return s
}

func main() {
	p := tea.NewProgram(model{}, tea.WithAltScreen())
	if _, err := p.Run(); err != nil {
		fmt.Fprintf(os.Stderr, "Alas, there's been an error: %v", err)
		os.Exit(1)
	}
}
