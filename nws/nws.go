package nws

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/jwtouron/weather-tui/util"
)

func fetchAndUnmarshal(url string, v any) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &v)
	util.Assert(err == nil, "Can't unmarshal JSON: %v", err)

	return nil
}

type measurementJSON  struct {
	UnitCode string
	Value interface{}
}

type Temperature struct {
	json measurementJSON
}

type Point struct {
	json pointJSON
}

type pointJSON struct {
	Properties pointPropertiesJSON
}

type pointPropertiesJSON struct {
	ForecastHourly string
	ObservationStations string
}

func FetchPoint(lat, long float64) (Point, error) {
	pointJSON := pointJSON{}

	err := fetchAndUnmarshal(fmt.Sprintf("https://api.weather.gov/points/%f,%f", lat, long), &pointJSON)
	if err != nil {
		return Point{}, err
	}
	return Point{pointJSON}, nil
}
