package nws

import (
	// "fmt"
	// "encoding/json"
	"time"
)

type PeriodForecast struct {
	json periodJSON
}

type hourlyForecastJSON struct {
	Properties struct {
		Periods []periodJSON
	}
}

type periodJSON struct {
	DewPoint measurementJSON
	EndTime time.Time
	RelativeHumidity measurementJSON
	ShortForecast string
	StartTime time.Time
	Temperature int
	TemperatureUnit string
}

func (point Point) FetchHourlyForecast() ([]PeriodForecast, error) {
	var json hourlyForecastJSON

	err := fetchAndUnmarshal(point.json.Properties.ForecastHourly, &json)
	if err != nil {
		return nil, err
	}
	// fmt.Printf("%+v\n", json)
	// bs, err := json.MarshalIndent(m, "", "  ")
	// fmt.Println(string(bs))

	periods := []PeriodForecast{}

	for _, period := range json.Properties.Periods {
		periods = append(periods, PeriodForecast{period})
	}

	return periods, nil
}
