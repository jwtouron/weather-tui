package nws

import (
	"fmt"
)

type observationStationsJSON struct {
	Features []observationStationJSON
}

func (point Point) FetchObservationStations() ([]ObservationStation, error) {
	observationStationsJSON := observationStationsJSON{}

	err := fetchAndUnmarshal(point.json.Properties.ObservationStations, &observationStationsJSON)
	if err != nil {
		return nil, err
	}

	stations := []ObservationStation{}
	for _, station := range observationStationsJSON.Features {
		stations = append(stations, ObservationStation{station})
	}
	return stations, nil
}

type ObservationStation struct {
	json observationStationJSON
}

type observationStationJSON struct {
	ID string
	Properties struct {
		StationIdentifier string
		Name string
	}
}

func (station ObservationStation) Name() string {
	return station.json.Properties.Name
}

func (station ObservationStation) StationIdentifier() string {
	return station.json.Properties.StationIdentifier
}

func (station ObservationStation) FetchLatestObservation() (LatestObservation, error) {
	observation := LatestObservation{}

	err := fetchAndUnmarshal(fmt.Sprintf("%s/observations/latest", station.json.ID), &observation.json)
	if err != nil {
		return LatestObservation{}, nil
	}

	return observation, nil
}

type LatestObservation struct {
	json struct {
		Properties struct {
			DewPoint measurementJSON
			HeatIndex measurementJSON
			RelativeHumidity measurementJSON
			Temperature measurementJSON
			TextDescription string
			WindChill measurementJSON
		}
	}
}

func (temp Temperature) Celsius() (float64, bool) {
	if f, ok := temp.json.Value.(float64); ok {
		if temp.json.UnitCode == "wmoUnit:degC" {
			return f, true
		} else {
			return (f - 32) / 1.8, true
		}
	} else {
		return 0, false
	}
}

func (temp Temperature) Fahrenheit() (float64, bool) {
	if f, ok := temp.json.Value.(float64); ok {
		if temp.json.UnitCode == "wmoUnit:degC" {
			return f * 1.8 + 32, true
		} else {
			return f, true
		}
	} else {
		return 0, false
	}
}

func (observation LatestObservation) DewPoint() Temperature {
	return Temperature{observation.json.Properties.DewPoint}
}

func (observation LatestObservation) HeatIndex() Temperature {
	return Temperature{observation.json.Properties.HeatIndex}
}

func (observation LatestObservation) RelativeHumidity() (float64, bool) {
	if f, ok := observation.json.Properties.RelativeHumidity.Value.(float64); ok {
		return f, true
	} else {
		return 0, false
	}
}

func (observation LatestObservation) Temperature() Temperature {
	return Temperature{observation.json.Properties.Temperature}
}

func (observation LatestObservation) TextDescription() string {
	return observation.json.Properties.TextDescription
}

func (observation LatestObservation) WindChill() Temperature {
	return Temperature{observation.json.Properties.WindChill}
}
